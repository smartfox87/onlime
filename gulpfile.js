'use strict';

const gulp = require('gulp');
const del = require('del');
const rename = require('gulp-rename');
const gulpIf = require('gulp-if');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const postcss = require('gulp-postcss');

let isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

let files = [
	'./source/img/**/*.*',
	'./source/upld/**/*.*',
];

gulp.task('img', function () {
	console.log('---------- перенос img и upld');
	return gulp.src(files, {base: './source'})
		.pipe(gulp.dest('skin/frontend/newsite_themes/default/'));
});

gulp.task('html', function () {
	console.log('---------- сборка HTML страниц');
	const include = require('gulp-file-include');
	return gulp.src('source/pages/*.html')
		.pipe(plumber({errorHandler: notify.onError()}))
		.pipe(include({
			prefix: '@@',
			basepath: '@file'
		}))
		.pipe(gulp.dest('skin/frontend/newsite_themes/default'));
});

// сборка стилевого файла SCSS
gulp.task('style', function () {
	console.log('---------- сборка CSS');
	const autoprefixer = require('autoprefixer');
	const mqpacker = require('css-mqpacker');
	const sortCSSmq = require('sort-css-media-queries');
	const cssnano = require('gulp-cssnano');
	const concatCss = require('gulp-concat-css');
	return gulp.src('source/css/*.css')
		.pipe(plumber({errorHandler: notify.onError()}))
		.pipe(concatCss('styles.css'))
		.pipe(postcss([
			autoprefixer(),
			mqpacker({sort: sortCSSmq.desktopFirst})
		]))
		.pipe(gulp.dest('skin/frontend/newsite_themes/default/css'))
		.pipe(cssnano())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('skin/frontend/newsite_themes/default/css'))
});

// компиляция ES6 js оптимизация и копирование скриптов js в папку сборки
gulp.task('js', function () {
	console.log('---------- компиляция ES6 js копирование скриптов в папку сборки');
	const uglify = require('gulp-uglify');
	const concat = require('gulp-concat');
	const babel = require('gulp-babel');
	return gulp.src(['source/js/jquery*.js', 'source/js/plugins/*.js', 'source/js/main.js'],
		{base: '/source'})
		.pipe(plumber({errorHandler: notify.onError()}))
		.pipe(babel({ignore: ['source/js/jquery*.js', 'source/js/plugins/*.js']}))
		.pipe(concat('js/script.js'))
		.pipe(gulp.dest('skin/frontend/newsite_themes/default'))
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('skin/frontend/newsite_themes/default'));
});

// запуск слежения за файлами
gulp.task('watch', function () {
	console.log('---------- запуск слежения за изменениями');
	gulp.watch('source/css/**/*.css', gulp.series('style'));
	gulp.watch('source/blocks/**/*.html', gulp.series('html'));
	gulp.watch('source/pages/*.html', gulp.series('html'));
	gulp.watch('source/js/**/*.js', gulp.series('js'));
	gulp.watch('source/img/**/*', gulp.series('img'));
});

// запуск сервера
gulp.task('serve', function () {
	console.log('---------- запуск сервера');
	const server = require('browser-sync').create();
	server.init({
		server: 'skin/frontend/newsite_themes/default/',
		index: 'index.html',
	});
	server.watch('skin/frontend/newsite_themes/default/css/*.css').on('change', server.reload);
	server.watch('skin/frontend/newsite_themes/default/*.html').on('change', server.reload);
	server.watch('skin/frontend/newsite_themes/default/js/*.js').on('change', server.reload);
});

// запуск разработки проекта
gulp.task('default',
	gulp.series(
		gulp.parallel('html', 'style', 'js', 'img'),
		gulp.parallel('watch', 'serve')));

gulp.task('production', gulp.parallel('html', 'style', 'js', 'img'))