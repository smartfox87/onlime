function customSelectFilterInit() {
	let initSelect = document.getElementsByClassName('js-custom__select');

	if (!initSelect) {
		return;
	}
	for (let i = 0; initSelect.length > i; i++) {
		let selectElem = initSelect[i].querySelector('.select');
		let customElem = initSelect[i].querySelector('.custom__select-wrap');
		let customElemOption = customElem.querySelector('.custom__select-drop-js');
		let optionsElement = selectElem.options;

		for (let a = 0; optionsElement.length > a; a++) {

			let templateOpt = document.createElement('span');
			templateOpt.classList.add('select-def');
			templateOpt.dataset.value = a;
			templateOpt.innerHTML = optionsElement[a].text;
			selectElem.style.display = 'none';
			customElemOption.append(templateOpt);
			if (selectElem.selectedIndex == a) {
				templateOpt.classList.add('active');
			}

		}

	}

}

function disableBtnCart() {
	let actionForm = document.querySelector('.cart__actions');
	if (!actionForm) {
		return
	}
	let inputCodeEl = document.querySelector('.input-code');
	let inputCodeBtn = document.querySelector('.js-btn-ap');
	let selectRegionEl = document.querySelector('.input-reg');
	let inputZipEl = document.querySelector('.input-zip');
	let radioBtn = actionForm.querySelectorAll('input[type=\'radio\']')
	let aggreCheckbox = actionForm.querySelector('#agree-check');
	let delivery;

	console.log('reinit');
	inputCodeEl.addEventListener('input', function () {
		if (inputCodeEl.value !== '') {
			inputCodeBtn.disabled = false;
		} else {
			inputCodeBtn.disabled = true;
		}
	})

	selectRegionEl.addEventListener('input', function () {
		// procedBtnActive(aggreCheckbox.checked)
	});

	inputZipEl.addEventListener('input', function () {
		// procedBtnActive(aggreCheckbox.checked)
	});

	document.addEventListener('input', function (e) {

		if (e.target && e.target.id === 'agree-check') {
			// let agreeBtn = e.target;
			procedBtnActive(e.target.checked);
		}
	})

	document.addEventListener('click', function (e) {

		if (e.target && e.target.classList.contains('toggle-radio') || e.target.classList.contains('estimate_method')) {

			// procedBtnActive(aggreCheckbox.checked);

		}

	});

	function procedBtnActive(aggree = aggreCheckbox.checked) {

		let aggreCheckbox = actionForm.querySelector('#agree-check');
		let ZipValuer = inputZipEl.value;
		let proceedBtn = document.querySelector('.js-btn-proc');
		for (let i = 0; radioBtn.length > i; i++) {
			if (radioBtn[i].checked) {
				delivery = true;
			}
		}

		if (ZipValuer !== '' && selectRegionEl.value !== '' && aggree && delivery) {
			// proceedBtn.disabled = false;
		} else {
			// proceedBtn.disabled = true;
		}
	}

	// procedBtnActive(aggreCheckbox.checked);
}

(function ($) {

	function dropdown() {
		var doc = jQuery(document.documentElement)
		jQuery('.dropdown__toggle').mouseenter(function () {
			if (!('ontouchstart' in document.documentElement)) {
				let $this = jQuery(this);
				let $target = $this.closest('.dropdown').find('.dropdown__menu').first();

				if ($this.attr('data-target') && $this.attr('data-target').length) {
					$target = jQuery('#' + $this.attr('data-target'));
				}

				if ($target) {
					$this.parent().addClass('open');
					$target.show();
				}
			}

		}).mouseleave(function () {
			if (!('ontouchstart' in document.documentElement)) {
				let $this = jQuery(this);
				let $target = $this.closest('.dropdown').find('.dropdown__menu').first();

				if ($this.attr('data-target') && $this.attr('data-target').length) {
					$target = jQuery('#' + $this.attr('data-target'));
				}

				let t = setTimeout(function () {
					$target.hide();
					$this.parent().removeClass('open');
				}, 100);

				$target.on('mouseenter', function () {
					$target.show();
					$this.parent().addClass('open');
					clearTimeout(t);
				}).on('mouseleave', function () {
					$target.hide();
					$this.parent().removeClass('open');
				})
			}

		}).click(function () {
			let $this = jQuery(this);
			let $parent = $this.closest('.dropdown');
			let $content = $parent.find('.dropdown__menu');

			jQuery('.dropdown').not($parent).removeClass('open')
			jQuery('.dropdown__menu').not($content).hide()
			$content.toggle()
			$parent.toggleClass('open');
			if ($parent.hasClass('open')) {
				doc.addClass('open-dropdown');
			} else {
				doc.removeClass('open-dropdown');
			}
		})
		doc.off('click.dropdown').on('click.dropdown', function (e) {
			if (!e.target.closest('.dropdown')) {
				doc.removeClass('open-dropdown');
				jQuery('.dropdown').removeClass('open')
				jQuery('.dropdown__menu').hide()
			}
		})
	}

	function mainMenu() {
		let $body = jQuery('body');
		let $container = jQuery('#js_nav');
		let $actionLink = jQuery('.js_nav__link[data-target]');
		let $subList = jQuery('.js__nav-sub');
		const OPENCLASSNAME = 'menu-open';
		const HOVERCLASSNAME = 'menu-enter';
		const ACTIVECLASSNAME = 'active';
		const ANIMDURATION = 200;
		const TIMEOUTDURATION = 200;
		let menuTimeout = null;
		let menuTimeout2 = null;
		let subTimeout = null;
		let bodyBgTimeout = null;
		let menuHvrTimeout = null;
		let menuLvTimeout = null;

		function closeMenu($link, $submenu) {
			if (!$link.is(':hover')) {
				$link.removeClass(ACTIVECLASSNAME);
				$submenu.stop().slideUp(ANIMDURATION / 2);
			}
		}

		$container
			.mouseenter(function () {
				if (jQuery(this).hasClass(HOVERCLASSNAME)) {
					clearTimeout(menuLvTimeout);
				} else {
					menuHvrTimeout = setTimeout(function () {
						$body.addClass(HOVERCLASSNAME);
					}, TIMEOUTDURATION / 2);
				}
			})
			.mouseleave(function () {
				clearTimeout(menuHvrTimeout);
				menuLvTimeout = setTimeout(function () {
					$body.removeClass(HOVERCLASSNAME);
				}, TIMEOUTDURATION / 2);
			})

		$actionLink
			.mouseover(function () {
				let $this = jQuery(this);
				let $target = jQuery('#' + $this.attr('data-target'));

				if ($target) {
					// if($target.hasClass(ACTIVECLASSNAME)) {
					//   clearTimeout(menuTimeout2);
					// }
					if ($body.hasClass(OPENCLASSNAME)) {
						clearTimeout(bodyBgTimeout);
					} else {
						$body.addClass(OPENCLASSNAME);
					}
					if ($this.hasClass(ACTIVECLASSNAME)) {
						$target.stop().slideDown(ANIMDURATION);
					} else {
						$this.addClass(ACTIVECLASSNAME);
						$target.slideDown(ANIMDURATION);
					}
				}
			})
			.mouseout(function () {
				let $this = jQuery(this);
				let $target = jQuery('#' + $this.attr('data-target'));

				if ($this.attr('data-target').length) {
					$target = jQuery('#' + $this.attr('data-target'));
				}

				menuTimeout = setTimeout(function () {
					closeMenu($this, $target);
				}, TIMEOUTDURATION);

				bodyBgTimeout = setTimeout(function () {
					$body.removeClass(OPENCLASSNAME);
				}, TIMEOUTDURATION);

			})

		$subList
			.mouseenter(function () {
				$body.addClass(OPENCLASSNAME);
				clearTimeout(bodyBgTimeout);
				clearTimeout(menuTimeout);
				jQuery(this).stop().slideDown(ANIMDURATION);

			}).mouseleave(function () {
			$body.removeClass(OPENCLASSNAME);
			closeMenu(jQuery('[data-target="' + this.id + '"]'), jQuery(this));
		});

		jQuery(document).on('mouseenter.updatePreview', '.nav-sub__item', function () {
			if (jQuery(this).attr('data-image') !== undefined) {
				jQuery(this).closest('.nav-sub__row').find('.nav-sub__media img').attr('src', jQuery(this).attr('data-image'));
			}
		}).on('mouseenter.updatePreview', '.nav-sub__title', function () {
			if (jQuery(this).attr('data-image') !== undefined) {
				jQuery(this).closest('.nav-sub__row').find('.nav-sub__media img').attr('src', jQuery(this).attr('data-image'));
			}
		})
	}

	function mainMenuResp() {
		let $body = jQuery('body');
		let $actionLink = jQuery('.js_nav__link[data-target]');
		let $subList = jQuery('.js__nav-sub');
		const OPENCLASSNAME = 'menu-open';
		const ACTIVECLASSNAME = 'active';

		function clearMenu() {
			$subList.css('display', '');
			$actionLink.removeClass(ACTIVECLASSNAME);
			$subList.removeClass(ACTIVECLASSNAME);
		}

		clearMenu();

		jQuery('.js_menu-toggler').click(function () {
			$body.toggleClass(OPENCLASSNAME);
			if (!$body.hasClass(OPENCLASSNAME)) {
				$body.trigger('menuClosed');
			}
		})

		$body.on('menuClosed', function () {
			clearMenu()
		})

		$actionLink
			.click(function (e) {
				let $this = jQuery(this);
				let $target = jQuery('#' + $this.attr('data-target'));

				e.preventDefault();

				if ($target) {
					$target.addClass(ACTIVECLASSNAME);
				}
			})

		jQuery('.js_submenu-close').click(function (e) {
			e.preventDefault();
			jQuery(this).closest('.js__nav-sub').removeClass(ACTIVECLASSNAME);
		})
	}


	function promoTimer() {
		function runTimer(end_time, $context) {
			if (!$context) {
				console.log('No $context found');
				return false;
			}

			function timer_go() {
				let n_time = Date.now();
				let diff = end_time - n_time;

				if (diff <= 0) return false;

				let left = diff % 1000;

				//секунды
				diff = parseInt(diff / 1000);
				let s = diff % 60;
				if (s < 10) {
					jQuery('.seconds_1', $context).text(0);
					jQuery('.seconds_0', $context).text(s);
				} else {
					jQuery('.seconds_1', $context).text(parseInt(s / 10));
					jQuery('.seconds_0', $context).text(s % 10);
				}
				//минуты
				diff = parseInt(diff / 60);
				var m = diff % 60;
				if (m < 10) {
					jQuery('.minutes_1', $context).text(0);
					jQuery('.minutes_0', $context).text(m);
				} else {
					jQuery('.minutes_1', $context).text(parseInt(m / 10));
					jQuery('.minutes_0', $context).text(m % 10);
				}
				//часы
				diff = parseInt(diff / 60);
				var h = diff % 24;
				if (h < 10) {
					jQuery('.hours_1', $context).text(0);
					jQuery('.hours_0', $context).text(h);
				} else {
					jQuery('.hours_1', $context).text(parseInt(h / 10));
					jQuery('.hours_0', $context).text(h % 10);
				}
				//дни
				var d = parseInt(diff / 24);
				if (d < 10) {
					jQuery('.days_1', $context).text(0);
					jQuery('.days_0', $context).text(d);
				} else {
					jQuery('.days_1', $context).text(parseInt(d / 10));
					jQuery('.days_0', $context).text(d % 10);
				}
				setTimeout(timer_go, left);
			}

			setTimeout(timer_go, 0);
		}

		let promoTimers = jQuery('.js-timer[data-finish]');

		promoTimers.each(function () {
			let $timer = jQuery(this);
			let endTime = $timer.attr('data-finish');
			if (endTime > 0) {
				runTimer(endTime, $timer);
			}
		})
	}

	function fasets() {

		jQuery(document).on('click', '.js_fst-toggle', function () {
			let $this = jQuery(this);
			let $container = $this.closest('.js_fst');
			let $content = jQuery('.js_fst-cntnt', $container);
			$container.toggleClass('open');
			$content.slideToggle(250);
		})

		jQuery(document).on('click', '.js_show-filters', function () {
			let $this = jQuery(this);
			let $content = $this.closest('.js_fst-cntnt');
			let $hidenList = jQuery('.faset-row-hiden', $content);

			$this.toggleClass('pressed');
			$hidenList.slideToggle(250);
		})

	}

	function range() {
		jQuery('.js_range').each(function (e) {
			let $this = jQuery(this);
			let $content = $this.closest('.js_range_cont');
			let inputMin = jQuery('.js_range_min', $content).first();
			let inputMax = jQuery('.js_range_max', $content).first();
			let min = $this.data('min') || 0;
			let max = $this.data('max');
			let valMin = inputMin.val();
			let valMax = inputMax.val();

			$this.slider({
				range: true,
				min: min,
				max: max,
				values: [valMin, valMax],
				slide: function (event, ui) {
					inputMin.val(ui.values[0]);
					inputMax.val(ui.values[1]);
				}
			});
		})

		jQuery('.js_range_min').on('change', function () {
			let $this = jQuery(this);
			let $content = $this.closest('.js_range_cont');
			let inputMax = parseInt(jQuery('.js_range_max', $content).first().val());
			let $range = jQuery('.js_range', $content).first();

			if ($this.val() < $range.slider('option', 'min')) {
				$this.val($range.slider('option', 'min'));
			}

			if ($this.val() < inputMax) {
				$range.slider('values', 0, $this.val());
			} else {
				$this.val(inputMax - 1);
				$range.slider('values', 0, inputMax - 1);
			}
		});

		jQuery('.js_range_max').on('change', function () {
			let $this = jQuery(this);
			let $content = $this.closest('.js_range_cont');
			let inputMin = parseInt(jQuery('.js_range_min', $content).first().val());
			let $range = jQuery('.js_range', $content).first();

			if ($this.val() > $range.slider('option', 'max')) {
				$this.val($range.slider('option', 'max'));
			}

			if ($this.val() > inputMin) {
				$range.slider('values', 1, $this.val());
			} else {
				$this.val(inputMin + 1);
				$range.slider('values', 1, inputMin + 1);
			}
		});
	}

	function itemLightBox() {
		let $moreImgCont = jQuery('#js_more_images');
		const DISCLASSNAME = 'slick-disabled';
		var mainImg = jQuery('#js_main_image')
		var mainImgLnk = jQuery('#js_full_image_lnk')

		if (!jQuery('js_image_box')) {
			return false;
		}

		jQuery('#js_main_img_nxt').on('click', function (event) {
			let nextEl = jQuery('#js_more_images .current').next();

			if (nextEl.length) {
				jQuery('#js_main_img_prv').removeClass(DISCLASSNAME);

				if ((nextEl.offset().left + (nextEl.width())) > ($moreImgCont.offset().left + $moreImgCont.width())) {
					$moreImgCont.find('.slick-next').not('.slick-disabled').click();
				}

				nextEl.find('a').click();
			}
		})

		jQuery('#js_main_img_prv').on('click', function (event) {
			let prevEl = jQuery('#js_more_images .current').prev();

			if (prevEl.length) {
				jQuery('#js_main_img_nxt').removeClass(DISCLASSNAME);

				if (prevEl.offset().left <= $moreImgCont.offset().left) {
					$moreImgCont.find('.slick-prev').not('.slick-disabled').click();
				}
				;

				prevEl.find('a').click();
			}
		})

		jQuery('a', $moreImgCont).on('click', function (event) {
			var $this = jQuery(this)
			var parent = $this.parent()

			event.preventDefault();

			if (parent.hasClass('current')) return;
			toggleImg($this)
		})
		toggleImg(jQuery('a', $moreImgCont).first())

		function toggleImg($this) {
			var parent = $this.parent()
			var img = $this.attr('href')
			var imgMob = $this.attr('data-mob-img')
			var url = $this.attr('data-full-size')
			var duration = 200;

			if (mainImgLnk.data('zoom') && (jQuery(window).width() >= 1004)) {
				mainImgLnk.data('zoom').destroy();
			}

			mainImg.stop().animate({opacity: 0}, duration, 'linear', function () {
				mainImg.attr('src', jQuery(window).width() >= 1004 ? img : imgMob);
				mainImgLnk.attr('href', url);
				mainImg.off('load.fadeIn').on('load.fadeIn', function () {
					mainImg.animate({opacity: 1}, duration, 'linear');
				})
				if (jQuery(window).width() >= 1004) {
					mainImgLnk.CloudZoom();
				}
			});

			parent.addClass('current').siblings().removeClass('current');
			jQuery('#js_main_img_prv, #js_main_img_nxt').removeClass(DISCLASSNAME);

			if (!parent.next().length) {
				jQuery('#js_main_img_nxt').addClass(DISCLASSNAME);
			} else {
				if ((parent.next().offset().left + (parent.next().width())) > ($moreImgCont.offset().left + $moreImgCont.width())) {
					$moreImgCont.find('.slick-next').not('.slick-disabled').click();
				}
			}
			if (!parent.prev().length) {
				jQuery('#js_main_img_prv').addClass(DISCLASSNAME);
			} else {
				if (parent.prev().offset().left <= $moreImgCont.offset().left) {
					$moreImgCont.find('.slick-prev').not('.slick-disabled').click();
				}
			}
		}
	}


	function loadImgs() {
		let $imgsList = jQuery('img[data-src]');
		let scrollTop;
		let clientHeight;

		if (!$imgsList.length) {
			return false;
		}

		function getOffset(elem) {
			if (elem.getBoundingClientRect) {
				return getOffsetRect(elem);
			} else {
				return getOffsetSum(elem);
			}
		}

		function getOffsetRect(elem) {
			var box = elem.getBoundingClientRect(),
				body = document.body,
				docElem = document.documentElement,
				scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop,
				scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft,
				clientTop = docElem.clientTop || body.clientTop || 0,
				clientLeft = docElem.clientLeft || body.clientLeft || 0,
				top = box.top + scrollTop - clientTop,
				left = box.left + scrollLeft - clientLeft;

			return {top: Math.round(top), left: Math.round(left)}
		}

		function getAttr(o, attr) {
			var result = (o.getAttribute && o.getAttribute(attr)) || null;

			if (!result) {
				var attrs = o.attributes,
					length = attrs.length;

				for (var i = 0; i < length; i++) {
					if (attrs[i].nodeName === attr) {
						result = attrs[i].nodeValue;
					}
				}
			}
			return result;
		};

		function replaceSrc($imgsList) {
			scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
			clientHeight = document.documentElement.clientHeight;

			$imgsList.each(function (index, el) {
				if (getOffset(el).top <= (clientHeight + scrollTop) && getOffset(el).top >= (scrollTop)) {
					el.setAttribute('src', getAttr(el, 'data-src'));
					el.removeAttribute('data-src');
				}
			})
		}

		replaceSrc($imgsList);

		window.onscroll = function (event) {
			$imgsList = jQuery('img[data-src]');
			replaceSrc($imgsList);
		}
	}

	function tabsSwitch() {
		var tbsAnimStopped = true;

		jQuery('.js_tabs .js_tab').on('click', function () {
			var tabBtn = jQuery(this),
				tabsArea = null,
				tabsContent = null,
				trgtTab = null,
				curTab = null,
				tbsDuration = 400,
				ACTIVECLASSNAME = 'current';

			if ((!tbsAnimStopped) || (tabBtn.hasClass('current'))) return;

			tabsArea = tabBtn.closest('.js_tabs'),
				tabsContent = tabsArea.find('.js_tabs_content'),
				trgtTab = tabsContent.find('#' + tabBtn.attr('data-target-id')),
				curTab = trgtTab.siblings('.' + ACTIVECLASSNAME);

			if (trgtTab.length == 0) {
				console.log('Target tab content " #' + tabBtn.attr('data-target-id') + ' " not exist');
				return;
			}

			tbsAnimStopped = false;

			tabBtn.siblings().removeClass(ACTIVECLASSNAME);
			trgtTab.siblings().removeClass(ACTIVECLASSNAME);
			tabsContent.css({height: tabsContent.outerHeight(), 'overflow': 'hidden', 'position': 'relative'});

			tabBtn.addClass(ACTIVECLASSNAME);
			tabsArea.trigger('switch');
			trgtTab.css({'position': 'absolute', top: 0, left: 0, width: '100%',})

			curTab.fadeOut(tbsDuration / 2, function () {
				trgtTab.fadeIn(tbsDuration / 2, function () {
					tabsContent.animate({height: trgtTab.outerHeight()}, 150, function () {
						tabsContent.css({'overflow': ''});
						trgtTab.css({'position': '', top: '', left: '', width: '',}).addClass('current');
						tabsContent.css({height: ''});
						tbsAnimStopped = true;
						tabsArea.trigger('switched');
						jQuery(window).trigger('resize');
					});
				});
			});
		});
	}

	function counter() {
		jQuery('.js_counter').each(function () {
			var $this, $counterInput, $counterMinus, $counterPlus, changeValue, maxValue, minValue;

			changeValue = function (direction) {
				var currentValue = parseInt($counterInput.val(), 10);
				if (direction) {
					return $counterInput.val(currentValue + 1 <= maxValue ? currentValue + 1 : maxValue);
				} else {
					return $counterInput.val(currentValue - 1 >= minValue ? currentValue - 1 : minValue);
				}
			};

			$this = jQuery(this);
			$counterPlus = $this.find('.js_counter_plus');
			$counterMinus = $this.find('.js_counter_minus');
			$counterInput = $this.find('.js_counter_inpt');
			minValue = $counterInput.data('min');
			maxValue = $counterInput.data('max');

			$counterPlus.on('click', function () {
				return changeValue(1);
			});

			$counterMinus.on('click', function () {
				return changeValue(0);
			});
		});
	};


	function popup() {
		var $document, activeClass;
		$document = jQuery(document);
		activeClass = 'popup_active';
		$document.on('click', '[data-popup]', function (e) {
			var target = jQuery(this).attr('data-popup');
			e.preventDefault();

			$document.trigger({
				type: 'openPopup',
				target: target,
			});
		});

		$document.on('openPopup', function (e) {
			var $targetPopup;
			$targetPopup = jQuery('#' + (e.target));

			if ($targetPopup.length) {
				$targetPopup.fadeIn().css('display', 'flex');
				return setTimeout(function () {
					$targetPopup.addClass(activeClass);
					return $targetPopup.scrollTop(0);
				}, 100);
			}
		});

		$document.on('mousedown', '.popup', closePopup);
		$document.on('touchstart', '.popup', closePopup);

		function closePopup(e) {
			var $target;
			$target = jQuery(e.target);
			if (!$target.closest('.popup__content').length || $target.is('.js--popup-close') || $target.closest('.js--popup-close').length) {
				return jQuery(this).removeClass(activeClass).fadeOut();
			}
		}

		return $document.keydown(function (e) {
			var $activePopup;
			$activePopup = jQuery('.' + activeClass);
			if (e.keyCode === 27 && $activePopup.length) {
				return $activePopup.removeClass(activeClass).fadeOut();
			}
		});
	};

	// переключение скрытого блока по кнопке
	function toggleFadeHover() {
		var allParents = jQuery('.js-fade')
		var allContents = allParents.find('.js-fade__content')

		allParents.each(function () {
			var parent = jQuery(this)
			var content = parent.find('.js-fade__content')
			var toggle = parent.find('.js-fade__toggle')

			// блокировка всплытия из контента
			content.click(function (event) {
				event.stopPropagation()
			})

			// клик по переключателю состояния
			toggle.off('mouseenter.toggleFadeHover').on('mouseenter.toggleFadeHover', toggleFadeHover)
			toggle.off('mouleaveut.toggleFadeHover').on('mouseleave.toggleFadeHover', toggleFadeHover)

			function toggleFadeHover(event) {
				event.preventDefault()
				event.stopPropagation()
				var toggle = jQuery(this)
				console.log(parent);
				if (toggle.data('text')) {
					content.html(toggle.data('text'))
					console.log(toggle.offset().left);
					console.log(parent.offset().left);
					content.css({left: toggle.offset().left - parent.offset().left})
				}

				// закрыть остальные блоки Dropdown
				allParents.not(parent).removeClass('open-fade')
				allContents.not(content).fadeOut(300)

				// переключение состояния
				if (!parent.hasClass('open-fade')) {
					openDropdown()
				} else {
					closeDropdown()
				}
			}

			// открыть
			function openDropdown() {
				parent.addClass('open-fade')
				content.fadeIn(300)
			}

			// закрыть
			function closeDropdown() {
				parent.removeClass('open-fade')
				content.fadeOut(300)
			}
		})
	}

	// триггер клика по другому или одновременно нескольким элементам
	function triggerClick() {
		var allSenders = jQuery('.js-trigger-click__sender')
		var allRecipients = jQuery('.js-trigger-click__recipient')

		allSenders.each(function () {
			var sender = jQuery(this)
			// массив из ID отправителя для связывания триггеров со своими приемщиками
			var triggerIdArr = (sender.data('triggerId')) ? String(sender.data('triggerId')).split(',') : false

			// обработка клика отправителя триггера
			sender.off('click.triggerClick').on('click.triggerClick', function (event) {
				event.stopPropagation()
				event.preventDefault()

				if (triggerIdArr) {
					// перебор масива приемщиков триггеров
					allRecipients.each(function () {
						const recipient = jQuery(this)

						// инициализация клика при совпадении ID отправителя и приемщика
						if (~triggerIdArr.indexOf(String(recipient.data('triggerId')))) {
							console.log('ddddddddddddd', String(recipient.data('triggerId')))
							console.log('ddddddddddddd', recipient)
							recipient.triggerHandler('mouseenter')
						}
					})
				}
			})
		})
	}

	function initSliders() {
		if (jQuery.fn.slick) {
			let slickBtnCodePr = '<button type="button" class="slick-btn slick-prev"></button>';
			let slickBtnCodeNx = '<button type="button" class="slick-btn slick-next"></button>';

			if (jQuery('#js_main-crsl').length) {
				jQuery('#js_main-crsl').slick({
					infinite: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					prevArrow: slickBtnCodePr,
					nextArrow: slickBtnCodeNx,
					dots: true,
					responsive: [
						{
							breakpoint: 767,
							settings: {
								arrows: false,
								// autoplay: true,
								// autoplaySpeed: 5000
							}
						}
					]
				});
			}

			// слайдер цветов на карточке товара
			jQuery('.js-color-list').each(function () {
				var slider = jQuery(this)
				var slides = slider.find('.js-color-list__item')

				if (slides.length > 5) {
					slider.slick({
						slidesToShow: 5,
						slidesToScroll: 1,
						prevArrow: slickBtnCodePr,
						nextArrow: slickBtnCodeNx,
						infinite: false,
						responsive: [
							{
								breakpoint: 480,
								settings: {
									slidesToShow: 6,
									swipeToSlide: true,
								}
							}
						]
					});
				}
			})

			jQuery('.js_blog-crsl').slick({
				infinite: false,
				slidesToShow: 3,
				slidesToScroll: 1,
				prevArrow: slickBtnCodePr,
				nextArrow: slickBtnCodeNx,
				dots: false,
				responsive: [
					{
						breakpoint: 1003,
						settings: {
							slidesToShow: 2,
							arrows: false,
							swipeToSlide: true,
							dots: true
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 1,
							arrows: false,
							swipeToSlide: true,
							dots: true,
							adaptiveHeight: true,
						}
					}
				]
			});

			jQuery('.js_cv_crsl').slick({
				infinite: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				// adaptiveHeight: true,
				prevArrow: slickBtnCodePr,
				nextArrow: slickBtnCodeNx,
				dots: true,
				responsive: [
					{
						breakpoint: 480,
						settings: {
							arrows: false,
							swipeToSlide: true
						}
					}
				]
			});

			jQuery('#js_more_crsl').slick({
				infinite: false,
				slidesToShow: 5,
				slidesToScroll: 1,
				prevArrow: slickBtnCodePr,
				nextArrow: slickBtnCodeNx,
				appendArrows: '#js_more_images',
				dots: false,
				responsive: [
					{
						breakpoint: 1440,
						settings: {
							slidesToShow: 3
						},
					},
					{
						breakpoint: 1003,
						settings: {
							slidesToShow: 5
						}
					},
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 3
						}
					},
					{
						breakpoint: 479,
						settings: {
							slidesToShow: 3.5,
							slidesToScroll: 1,
							swipeToSlide: true,
							dots: true,
						}
					},
					{
						breakpoint: 380,
						settings: {
							slidesToShow: 2.5,
							slidesToScroll: 1,
							swipeToSlide: true,
							dots: true,
						}
					}
				]
			});

			jQuery('.js_crsl').each(function () {
				var crslNavCont = jQuery(this).parent();
				jQuery(this).slick({
					infinite: true,
					slidesToShow: 5,
					slidesToScroll: 5,
					prevArrow: slickBtnCodePr,
					nextArrow: slickBtnCodeNx,
					appendArrows: crslNavCont,
					dots: false,
					responsive: [
						{
							breakpoint: 1440,
							settings: {
								slidesToShow: 4,
								slidesToScroll: 4,
								swipeToSlide: true
							}
						},
						{
							breakpoint: 1003,
							settings: {
								slidesToShow: 3,
								slidesToScroll: 3,
								swipeToSlide: true
							}
						},
						{
							breakpoint: 767,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 2,
								arrows: false,
								dots: true,
								swipeToSlide: true
							}
						},
						{
							breakpoint: 480,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								swipeToSlide: true,
								dots: true,
								arrows: false
							}
						}
					]
				});
			});

			jQuery('.js_crsl_portfolio').each(function () {
				jQuery(this).slick({
					infinite: false,
					dots: false,
					arrows: false,
					swipeToSlide: false,
					draggable: false,
					initialSlide: 0,
					responsive: [
						{
							breakpoint: 480,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								swipeToSlide: true,
								dots: true,
								arrows: false
							}
						}
					]
				});
			});

			jQuery('.js_crsl_style').each(function () {
				jQuery(this).slick({
					infinite: false,
					dots: false,
					arrows: false,
					swipeToSlide: false,
					draggable: false,
					initialSlide: 0,
					responsive: [
						{
							breakpoint: 480,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								swipeToSlide: true,
								dots: true,
								arrows: false
							}
						}
					]
				});
			});
		}
	}

// Видимости элемента после указанной высоты скролла страницы
	function showElemAfterScroll(element, toggleHeightScroll) {
		var doc = jQuery(document)

		doc.off('scroll.showElemAfterScroll').on('scroll.showElemAfterScroll', toggleShow)
		doc.trigger('scroll.showElemAfterScroll')

		function toggleShow() {
			var scrollDocument = doc.scrollTop();

			if (scrollDocument < toggleHeightScroll) {
				element.fadeOut(300)
			} else {
				element.fadeIn(300)
			}
		}
	}

// подскролл страницы в начала по клику на кнопку
	function scrollTop() {
		var btn = jQuery('.js-scroll-top')
		showElemAfterScroll(btn, jQuery(window).height() / 2)

		btn.off('click.scrollTop').on('click.scrollTop', function (event) {
			event.preventDefault()
			jQuery([document.documentElement, document.body]).animate({scrollTop: 0}, 400)
		})
	}

	// очистить значение поля
	function clearInput() {
		const parents = document.querySelectorAll('.js-input-clear')
		Object.keys(parents).forEach(function (key) {
			const input = parents[key].querySelector('.js-input-clear__input')
			const clear = parents[key].querySelector('.js-input-clear__clear')

			clear.addEventListener('click', function () {
				input.value = ''
				parents[key].classList.remove('form-control--clear')
				parents[key].classList.remove('form-control--success')
			})

			input.addEventListener('keyup', function () {
				if (!input.value.length) {
					parents[key].classList.remove('form-control--clear')
					parents[key].classList.remove('form-control--success')
				}
			})
		})
	}

	// переключение видимости блока по чекбоксу (не требует переинициализации после аякс)
	function toggleCheckboxBlock() {
		jQuery(document)
			.off('change.toggleCheckboxBlock', '.js-checkbox-block__input')
			.on('change.toggleCheckboxBlock', '.js-checkbox-block__input', toggleCheckboxBlock)

		function toggleCheckboxBlock() {
			// проверяет явлеяется ли this елементом инициатора события или window
			var input = this.location ? jQuery('.js-checkbox-block__input') : jQuery(this)
			var content = input.closest('.js-checkbox-block').find('.js-checkbox-block__content')

			if (input.prop('checked')) {
				content.fadeIn(300)
			} else {
				content.fadeOut(300)
			}
		}

		toggleCheckboxBlock()
	}

	// маска для полей ввода
	function maskInputs() {
		if (jQuery.fn.inputmask) {
			jQuery('.js-mask').each(function () {
				var input = jQuery(this)
				input.inputmask(input.data('mask'), {
					'clearIncomplete': true,
					'placeholder': '_',
					'showMaskOnHover': false,
				})
			})
		}
	}

	function loadMap() {
		var $map = jQuery('#usa-map'), //svg jquery
			svg = document.querySelector('#usa-map'),
			$container = jQuery('.map-container');


		var paths = d3.select(svg).selectAll('path');
		//on resize and initial (need to account for css)

		//get all the paths of the svg, for each
		paths.each(function (d, i) {

			var classSelected = d3.select(this).attr('class');
			var stateSelected = classMaps[classSelected];

			//check to see if the svg object is a state from our json
			var isState = (typeof stateSelected == 'object') ? true : false;

			//if state is in our json list, then
			if (isState) {

				//fill in color-- can put whatever you want
				d3.select(this).style('fill', 'rgba(45, 204, 112, .28)');

				//add a glowing orb to map with js
				var $d = jQuery('<div></div>');
				$d.addClass('glowing-orb').addClass(stateSelected.name.toLowerCase().replace(/\s/g, ''));
				$container.append($d);

			}

			//if we hover over any location on the map, then get path and if state, fill color red
			//jquery hover, two handlers for one on mouseenter and one for mouseleave
			jQuery(this).hover(function () {
				var path = jQuery(this)[0];

				if (isState) d3.select(this).style('fill', 'rgba(45, 204, 112, .6)');
			}, function () {
				var path = jQuery(this)[0];
				if (isState) d3.select(path).style('fill', 'rgba(45, 204, 112, .28)');
			}).css('cursor', 'pointer');

			//on click of svg, create the tooltip
			d3.select(this).on('click touchstart', function () {
				var classSelected = d3.select(this).attr('class');
				if (classSelected) {
					toolTip(classSelected);
				}
			});
		}); //end paths for each

		//handles tooltip creation
		function toolTip(classSel) {

			//again if the class matches up to our json list, then we want to do something
			var stateSelected = classMaps[classSel];
			var isState = (typeof stateSelected == 'object') ? true : false;

			var $h3 = jQuery('.js-map-item__caption'),
				$values = jQuery('.js-map-item__value');
			$link = jQuery('.js-map-item__link');

			//show tooltip if not mobile -- can be improved if desired
			if (isState && window.innerWidth > 0) {
				//append each item in our stateSelected array as a list item
				for (var i = 0; i < stateSelected.list.length; i++) {
					if (i <= 1) {
						$values.eq(i).text(stateSelected.list[i])
					}
					if (i == 2) {
						$link.html(stateSelected.list[i])
					}
				}

				$h3.html(stateSelected.name);
			}
		}//end tooltip func
	}

	// accordion group
	function toggleAccordionGroup() {
		jQuery('.js-accordion--group').each(function () {
			var allAccordions = jQuery(this).find('.js-accordion__item')
			var allContents = allAccordions.find('.js-accordion__content')

			allAccordions.each(function () {
				var accordion = jQuery(this)
				var content = accordion.find('.js-accordion__content')

				// инициализация и закрытие всех аккордионов кроме тех что с классом open-accordion
				if (!accordion.hasClass('open-accordion')) content.slideUp(0)
				accordion.addClass('init-accordion')

				// переключение состояния аккордиона по клику
				accordion.find('.js-accordion__toggler').off('click.toggleAccordion').on('click.toggleAccordion', function (event) {
					event.stopPropagation()
					event.preventDefault()

					closeOtherAccordions()

					// переключение состояния аккордиона
					accordion.toggleClass('open-accordion')
					content.slideToggle(300)
				})

				// закрытие остальных аккардионов в группе
				function closeOtherAccordions() {
					allAccordions.not(accordion).removeClass('open-accordion')
					allContents.not(content).slideUp(300)
				}
			})
		})
	}

	// accordion only adaptive
	function toggleAccordionOnlyAdaptive() {
		var timeout = null
		var win = jQuery(window)
		var doc = jQuery(document)

		// accordion только для адаптива
		function toggleAccordionOnlyAdaptive() {
			var allParents = jQuery('.js-accordion--adaptive')

			allParents.each(function () {
				var parent = jQuery(this)
				var content = parent.find('.js-accordion__content')
				var toggler = parent.find('.js-accordion__toggler')
				var maxWidth = parseInt(parent.data('maxWidth'))

				if (win.width() <= maxWidth) {
					// инициализация и закрытие всех аккордионов кроме тех что с классом open-accordion
					if (!parent.hasClass('open-accordion')) content.slideUp(0)
					parent.addClass('init-accordion')
					parent.removeClass('destroy-accordion')

					toggler.off('click.toggleAccordion').on('click.toggleAccordion', function (event) {
						event.stopPropagation()
						event.preventDefault()

						parent.toggleClass('open-accordion')
						content.slideToggle(300)
					})
				} else {
					// accordion destroy
					parent.removeClass('init-accordion')
					parent.removeClass('open-accordion')
					parent.addClass('destroy-accordion')
					content.slideDown(0)

					toggler.off('click.toggleAccordion')
				}
			})
		}

		win.off('resize.toggleAccordionOnlyAdaptive').on('resize.toggleAccordionOnlyAdaptive', function () {
			clearTimeout(timeout);
			timeout = setTimeout(toggleAccordionOnlyAdaptive, 300);
		})

		win.trigger('resize.toggleAccordionOnlyAdaptive');
	}

	// фиксация блока при скролле страницы
	function fixedBlockWhenScrolling() {
		jQuery('.js-fixed').each(function (index) {
			var block = jQuery(this)
			var content = block.find('.js-fixed__content')
			var blockHeight = content.outerHeight()
			var blockStart = block.offset().top
			var doc = jQuery(document)
			var win = jQuery(window)

			function fixedBlockWhenScrolling() {
				var docScroll = doc.scrollTop()

				// фиксация блока
				block.css('padding-top', blockHeight)
				if (!block.hasClass('active-fixed') && blockStart <= docScroll) {
					doc.addClass('active-fixed')
					block.addClass('active-fixed')
				} else if (block.hasClass('active-fixed') && blockStart > docScroll) {
					// отмена фиксации блока
					doc.removeClass('active-fixed')
					block.removeClass('active-fixed')
					// block.removeClass('active-fixed').css('padding-top', 0)
				}
			}

			// инициализаци с индексом на случай множественных блоков для фиксации
			win.off('resize.fixedBlockWhenScrolling' + index)
				.on('resize.fixedBlockWhenScrolling' + index, function () {
					blockHeight = content.outerHeight()
					blockStart = block.offset().top
					block.css('padding-top', blockHeight)

					if (requestAnimationFrame) requestAnimationFrame(fixedBlockWhenScrolling)
					else fixedBlockWhenScrolling();
				})

			// инициализаци с индексом на случай множественных блоков для фиксации
			doc.off('scroll.fixedBlockWhenScrolling' + index)
				.on('scroll.fixedBlockWhenScrolling' + index, function () {
					if (requestAnimationFrame) requestAnimationFrame(fixedBlockWhenScrolling)
					else fixedBlockWhenScrolling();
				})
				.trigger('scroll.fixedBlockWhenScrolling' + index);
		})
	}

	// блокировка кнопки по незаполненным полям
	function toggleDisabledBtn() {
		jQuery('.js-toggle-disabled').each(function () {
			var $parent = jQuery(this);
			var $inputs = $parent.find('.js-toggle-disabled__input');
			var $btn = $parent.find('.js-toggle-disabled__btn');

			$inputs.off('change.toggleDisabledBtn').on('change.toggleDisabledBtn', toggleDisabledBtn);
			$inputs.off('input.toggleDisabledBtn').on('input.toggleDisabledBtn', toggleDisabledBtn);

			toggleDisabledBtn();

			// проверка всех полей на заполненость и переключение блокировки кнопки
			function toggleDisabledBtn() {
				// все заполненные поля
				var inputsTrue = jQuery.grep($inputs, function (item) {
					var input = jQuery(item);
					var status = false;

					// проверка значения поля согласно его типу
					switch (input.prop('type')) {
						case 'text':
							status = input.val().length;
							break;
						case 'checkbox':
							status = input.prop('checked');
							break;
					}
					return status;
				})

				// блокировка кнопки если не все поля заполнены
				var blockedState = $inputs.length !== inputsTrue.length;

				$btn.prop('disabled', blockedState);
				$parent.toggleClass('_disabled', blockedState);
			}
		})
	}

	// отслеживание свайпа на мобилке для галереи
	function detectSwipe() {
		let touchstartX = 0;
		let touchstartY = 0;
		let touchendX = 0;
		let touchendY = 0;

		const gestureZone = document.getElementById('js_full_image_lnk');

		if (gestureZone) {
			gestureZone.addEventListener('touchstart', function (event) {
				touchstartX = event.changedTouches[0].screenX;
				touchstartY = event.changedTouches[0].screenY;
			}, false);

			gestureZone.addEventListener('touchend', function (event) {
				touchendX = event.changedTouches[0].screenX;
				touchendY = event.changedTouches[0].screenY;
				handleGesture();
			}, false);
		}

		function handleGesture() {
			if (touchendX < touchstartX) {
				console.log('Swiped left');
				jQuery('.more-img.current').next().find('a').click()
			}

			if (touchendX > touchstartX) {
				jQuery('.more-img.current').prev().find('a').click()
				console.log('Swiped right');
			}

			if (touchendY < touchstartY) {
				console.log('Swiped up');
			}

			if (touchendY > touchstartY) {
				console.log('Swiped down');
			}

			if (touchendY === touchstartY) {
				console.log('Tap');
			}
		}
	}

	// переключение класса выбора товара в заказе
	function toggleSelectedItem() {
		jQuery('.toggle__input').change(function () {
			var input = jQuery(this)
			input.closest('.cart__item').toggleClass('cart__item--selected', input.prop('checked'))
		})
	}

	function toggleAdaptiveBg() {
		var win = jQuery(window)
		if (win.width() <= 480) {
			jQuery('[data-bg-mobile]').each(function () {
				var item = jQuery(this)
				item.css({backgroundImage: 'url(' + item.data('bg-mobile') + ')'})
			})
		}
	}


	function buildFullPage() {
		var fullpageConrainer = jQuery('.js-fullpage__content');

		if ($.fn.fullpage && fullpageConrainer.length) {
			function buildFullPage() {
				fullpageConrainer.fullpage({
					menu: '.js-fullpage__nav',
					verticalCentered: false,
					fitToSection: false,
				});
			}

			jQuery(window).off('resize.buildFullPage').on('resize.buildFullPage', function () {
				if (window.innerWidth >= 1024) {
					buildFullPage();

					jQuery('.js-fullpage__nav-item').off('click.buildFullPage').on('click.buildFullPage', function () {
						jQuery.fn.fullpage.moveTo(jQuery(this).attr('data-menuanchor'));
					});
				} else {
					if (window.fullpage_api) {
						fullpage_api.destroy()
						jQuery('.fp-section').css({height: 'auto'})
					}
				}
			}).triggerHandler('resize.buildFullPage')
		}
	}

	// блокировать всплытие события скролла на элементе
	function lockScroll() {
		if (jQuery.fn.scrollLock) {
			jQuery('.js-lock-scroll, .solutions-page .aside-left').scrollLock({
				unblock: '.js-lock-scroll__unlock'
			});
		}
	}

	function filterPortfolio() {
		if (jQuery.fn.isotope) {
			$('.js-portfolio').each(function () {
				var parent = $(this)

				var grid = parent.find('.js-portfolio__list').isotope({
					itemSelector: '.js-portfolio__item',
					masonry: {
						percentPosition: true
					}
				});
				jQuery(document).on('click.isotope', '.js-portfolio__toggles', function () {
					jQuery(this).addClass('is-checked').siblings().removeClass('is-checked');
					grid.isotope({
						filter: jQuery(this).data('filter')
					});
				});
				setTimeout(function () {
					grid.isotope({
						filter: '*'
					});
				}, 0);
			})
		}
	}

	jQuery('document').ready(function () {

		jQuery('.js_chosen').each(function (index, el) {
			if (typeof chosen !== 'undefined') {
				jQuery(this).chosen({
					disable_search_threshold: 12
				});
			}
		});

		let fooTitles = document.getElementsByClassName('foo__nav_title');
		for (var i = 0; i < fooTitles.length; i++) {
			fooTitles[i].onclick = function () {
				if (this.parentNode.classList.contains('foo__nav-open')) {
					this.parentNode.classList.remove('foo__nav-open');
				} else {
					this.parentNode.classList.add('foo__nav-open');
				}
			}
		}

		jQuery(document).on('click', '.js_item_spec_tgl', function () {
			jQuery(this).closest('.cart__item_spec, .calc__reslts').toggleClass('opened');
		})

		jQuery('#select_all').click(function () {
			if (jQuery(this).prop('checked') == true) {
				jQuery('.cart__list').find('.toggle__input').prop('checked', true);
			} else {
				jQuery('.cart__list').each(function (e) {
					jQuery(this).find('.toggle__input').prop('checked', false);
				});
			}
		})

		jQuery('#empty_cart').click(function () {
			if (jQuery('#select_all').prop('checked') == true) {

			}
		})

		jQuery(document).on('click', '.js_fasets-toggler', function () {
			jQuery('body').toggleClass('fasets-open');
		})

		jQuery('.js__search__tgl').click(function () {
			jQuery('body').toggleClass('search-open');
		})

		enquire.register('screen and (max-width: 1003px)', {
			match: function () {
				mainMenuResp()
			}
		}).register('screen and (min-width: 1004px)', {
			match: function () {
				mainMenu();

				// let cart_els = document.getElementsByClassName('cart__item_spec');
				// if(cart_els.length) {
				//   for (var i = cart_els.length - 1; i >= 0; i--) {
				//     cart_els[i].style.height = null;
				//   }
				// }
			}
		});
		let cardPage = document.querySelector('.cart') || '';
		let colorElem = document.querySelector('.color__elem-wrap') || '';

		function showDelBtn() {

			let allCheckBox = document.getElementsByClassName('checkbox-el');
			let showBtn = document.querySelector('.js-dell-all');
			let cartItem = document.querySelector('.cart__item');
			let allControlerBtn = document.querySelector('.all-control');

			if (cartItem) {
				[].forEach.call(allCheckBox, function (item, i) {
					item.addEventListener('input', function () {
						let arr = [];
						let allCart = document.getElementsByClassName('one-target');

						if (item.classList.contains('all-control')) {

							for (let i = 0; allCheckBox.length > i; i++) {
								if (item.checked) {
									allCheckBox[i].checked = true;
									showBtn.classList.add('show-btn');
								} else {
									allCheckBox[i].checked = false;
									showBtn.classList.remove('show-btn');
								}
								jQuery(allCheckBox[i]).trigger('change')
							}

						}
						for (let i = 0; allCheckBox.length > i; i++) {

							if (allCheckBox[i].checked && !allCheckBox[i].classList.contains('all-control')) {
								arr.push(allCheckBox[i].checked);
							}

						}
						if (arr.length === allCart.length) {
							allControlerBtn.checked = true;
						} else {
							allControlerBtn.checked = false;
						}
						if (arr.length > 0) {
							showBtn.classList.add('show-btn');
						} else {
							showBtn.classList.remove('show-btn');
						}

					})
				});
			}
		}

		function hoverToRemoveActive() {
			let colorBox = document.getElementsByClassName('color__wrap');

			[].forEach.call(colorBox, function (item, i) {
				item.addEventListener('mouseover', function () {
					let activeElem = item.querySelector('.color__elem-wrap.active');
					activeElem.classList.add('hide');
				})
			});
			[].forEach.call(colorBox, function (item, i) {
				item.addEventListener('mouseout', function () {
					let activeElem = item.querySelector('.color__elem-wrap.active');
					activeElem.classList.remove('hide');
				})
			})
		}

		function customSelectCart() {
			let select = document.querySelector('.custom__select') || '';
			if (!select) {
				return false
			}
			let selectBtn = document.querySelector('.custom__select-el');
			let dropDownSelect = document.querySelector('.custom__select-drop');
			let selectElem = document.getElementsByClassName('select-opt');
			let originSelect = document.getElementById('printFormAction');
			selectBtn.addEventListener('click', function () {

				dropDownSelect.classList.toggle('active');

			});

			document.addEventListener('click', function (e) {
				if (!e.target.classList.contains('custom__select') &&
					!e.target.classList.contains('custom__select-el') &&
					!e.target.classList.contains('custom__select-val') &&
					!e.target.classList.contains('select-btn') &&
					!e.target.classList.contains('select-opt')) {
					dropDownSelect.classList.remove('active');
				}
			});
			[].forEach.call(selectElem, function (item, i) {
				item.addEventListener('click', function () {

					dropDownSelect.classList.remove('active');
					originSelect.options[i].selected = true;
					setForm(originSelect);

					function setForm(el) {
						if (jQuery(el).children('option:selected').val()) {
							jQuery('#guest-form-order').submit();
							jQuery(el).prop('selectedIndex', 0);
						}
					}

				})
			})
		}

		let observer = new MutationObserver(customSelectFilterInit);
		let node = document.querySelector('.filters__list');
		if (node) {
			observer.observe(node, {
				childList: true,
			});
		}

		jQuery('body').triggerHandler('build');

		function customSelectEvent() {

			let initSelect = document.getElementsByClassName('js-custom__select'),
				allDropDownEl = document.getElementsByClassName('custom__select-drop-js'),
				allBtn = document.getElementsByClassName('custom__select-js'),
				allCustomSelect = document.getElementsByClassName('custom__select-val-js'),
				allBtnElement = document.getElementsByClassName('select-btn-js');
			if (!initSelect) {
				return;
			}
			document.addEventListener('click', function (e) {

				if (e.target && e.target.classList.contains('custom__select-js') ||
					e.target.classList.contains('custom__select-val-js') ||
					e.target.classList.contains('select-btn-js')) {
					let elemTarget = e.target.closest('.js-custom__select');
					let elemDropDown = elemTarget.querySelector('.custom__select-drop-js');
					let activeBtn = elemTarget.querySelector('.custom__select-js');
					elemDropDown.classList.toggle('active');
					activeBtn.classList.toggle('open');
					for (let i = 0; allDropDownEl.length > i; i++) {
						if (allBtn[i] !== e.target && allCustomSelect[i] !== e.target && allBtnElement[i] !== e.target) {
							allDropDownEl[i].classList.remove('active');
							allBtn[i].classList.remove('open');
						}
					}
				} else {
					for (let i = 0; allDropDownEl.length > i; i++) {
						allDropDownEl[i].classList.remove('active');
						allBtn[i].classList.remove('open');
					}
				}

			})

			document.addEventListener('click', function (e) {

				if (e.target.classList.contains('custom__select-drop-js') || e.target.classList.contains('select-def')) {
					let parentElem = e.target.closest('.js-custom__select');
					let value = e.target.textContent;
					let elemValue = parentElem.querySelector('.custom__select-val-js');
					let selectElem = parentElem.querySelector('.select');

					let allOptions = e.target.parentElement.querySelectorAll('.select-def')
					Object.keys(allOptions).forEach(function (key) {
						allOptions[key].classList.remove('active')
					})
					e.target.classList.add('active')

					let indexActiveElem = e.target.dataset.value;
					selectElem.selectedIndex = indexActiveElem;
					elemValue.innerText = value;
					jQuery(selectElem).trigger('change');

				}
			})
		}

		customSelectEvent();
		customSelectFilterInit();
		customSelectCart();
		if (colorElem) {
			hoverToRemoveActive()
		}
		if (cardPage) {
			disableBtnCart();
			showDelBtn();
		}
		dropdown();
		fasets();
		range();
		loadImgs();
		promoTimer();
		tabsSwitch();
		itemLightBox();
		counter();
		popup();
		toggleFadeHover()
		initSliders()
		triggerClick()
		scrollTop()
		toggleCheckboxBlock()
		maskInputs()
		loadMap();
		toggleAccordionGroup()
		fixedBlockWhenScrolling()
		// toggleDisabledBtn()
		detectSwipe()
		toggleSelectedItem()
		toggleAdaptiveBg()
		toggleAccordionOnlyAdaptive()
		buildFullPage()
		lockScroll()
		filterPortfolio()
	});


	if (window.$j) {
		$j(document).on('m-ajax-after', function () {
			disableBtnCart();
			counter();

			jQuery('.js_chosen').chosen({disable_search_threshold: 12})
			customSelectFilterInit()
			jQuery([document.documentElement, document.body]).animate({scrollTop: jQuery('.ctlg__filters').offset().top}, 400)
		});
	}
})(jQuery);



